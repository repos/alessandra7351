#!/bin/sh

hdspconf&
hdspmixer&
urxvtc -e sh -c 'jackd -dalsa -dhw:1,0 -r48000 -p512 -n2'
sleep 1s
pd -outchannels 4 -lib zexy -jack -stderr sea.pd
